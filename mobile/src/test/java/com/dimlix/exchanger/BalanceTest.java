package com.dimlix.exchanger;

import com.dimlix.exchanger.model.Currency;
import com.dimlix.exchanger.model.Rates;
import com.dimlix.exchanger.network.RatesUpdater;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import io.realm.Realm;
import rx.Observable;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@PrepareForTest({Realm.class})
public class BalanceTest {

    public static final float USD_RUB = 30f;
    public static final float EUR_USD = 1.10f;
    public static final float EUR_RUB = 40f;

    Realm mockRealm;

    @Rule
    public PowerMockRule rule = new PowerMockRule();
    private BalanceManager mBalanceManager;

    @Before
    public void setup() {
        mockRealm = MockSupport.mockRealm();

        Rates mockRates = PowerMockito.mock(Rates.class);
        when(mockRates.isUpToDate()).thenReturn(true);
        when(mockRates.isValid(RuntimeEnvironment.application)).thenReturn(true);

        when(mockRates.getUsdToRub()).thenReturn(USD_RUB);
        when(mockRates.getEurToRub()).thenReturn(EUR_RUB);
        when(mockRates.getUsdToEur()).thenReturn(EUR_USD);

        RatesUpdater mockRatesUpdated = PowerMockito.mock(RatesUpdater.class);
        when(mockRatesUpdated.getRates()).thenReturn(Observable.just(mockRates));

        mBalanceManager = new BalanceManager(RuntimeEnvironment.application, mockRealm, mockRatesUpdated);
        mBalanceManager.startRatesUpdater();
        mBalanceManager.getRatesObserveable().subscribe();
    }

    @Test
    public void realmShouldBeMocked() {
        assertThat(Realm.getDefaultInstance(), is(mockRealm));
    }

    @Test
    public void exchangeRates() throws Exception {
        float rub = mBalanceManager.getTotal(Currency.RUB);
        float usd = mBalanceManager.getTotal(Currency.USD);
        float eur = mBalanceManager.getTotal(Currency.EUR);

        Assert.assertEquals(BalanceManager.INITIAL_RUB + BalanceManager.INITIAL_USD * 30 + BalanceManager.INITIAL_EUR * 40, rub);
        Assert.assertEquals(BalanceManager.INITIAL_RUB / 30 + BalanceManager.INITIAL_USD + BalanceManager.INITIAL_EUR * 1.10f, usd);
        Assert.assertEquals(BalanceManager.INITIAL_RUB / 40 + BalanceManager.INITIAL_USD / 1.10f + BalanceManager.INITIAL_EUR, eur);
    }

    @Test
    public void geBalance() {
        float rub = mBalanceManager.getCurrencyBalance(Currency.RUB);
        float usd = mBalanceManager.getCurrencyBalance(Currency.USD);
        float eur = mBalanceManager.getCurrencyBalance(Currency.EUR);

        Assert.assertEquals(BalanceManager.INITIAL_RUB, rub);
        Assert.assertEquals(BalanceManager.INITIAL_USD, usd);
        Assert.assertEquals(BalanceManager.INITIAL_EUR, eur);
    }

    @Test
    public void testRubToUsdRate() {
        float rates = mBalanceManager.getRates(Currency.RUB, Currency.USD);
        Assert.assertEquals(1f / USD_RUB, rates);
    }

    @Test
    public void testRubToEurRate() {
        float rates = mBalanceManager.getRates(Currency.RUB, Currency.EUR);
        Assert.assertEquals(1f / EUR_RUB, rates);
    }

    @Test
    public void testUsdToRubRate() {
        float rates = mBalanceManager.getRates(Currency.USD, Currency.RUB);
        Assert.assertEquals(USD_RUB, rates);
    }

    @Test
    public void testUsdToEurRate() {
        float rates = mBalanceManager.getRates(Currency.USD, Currency.EUR);
        Assert.assertEquals(1f/ EUR_USD, rates);
    }

    @Test
    public void testEurToUsdRate() {
        float rates = mBalanceManager.getRates(Currency.EUR, Currency.USD);
        Assert.assertEquals(EUR_USD, rates);
    }

    @Test
    public void testEurToRubRate() {
        float rates = mBalanceManager.getRates(Currency.EUR, Currency.RUB);
        Assert.assertEquals(EUR_RUB, rates);
    }

    @Test
    public void textEchangeValid() {
        boolean exchange = mBalanceManager.exchange(Currency.RUB, Currency.USD, BalanceManager.INITIAL_RUB);
        Assert.assertTrue(exchange);
        float balance = mBalanceManager.getCurrencyBalance(Currency.RUB);
        Assert.assertEquals(0f, balance);
    }

    // Test valid exchanges

    @Test
    public void textEchangeRubToUsd() {
        int valueToExchange = 30000;
        boolean exchange = mBalanceManager.exchange(Currency.RUB, Currency.USD, valueToExchange);
        Assert.assertTrue(exchange);
        float newRub = mBalanceManager.getCurrencyBalance(Currency.RUB);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.USD);
        Assert.assertEquals(BalanceManager.INITIAL_RUB - valueToExchange, newRub);
        Assert.assertEquals(BalanceManager.INITIAL_USD + valueToExchange / USD_RUB, newUsd);
    }

    @Test
    public void textEchangeRubToEur() {
        int valueToExchange = 40000;
        boolean exchange = mBalanceManager.exchange(Currency.RUB, Currency.EUR, valueToExchange);
        Assert.assertTrue(exchange);
        float newRub = mBalanceManager.getCurrencyBalance(Currency.RUB);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.EUR);
        Assert.assertEquals(BalanceManager.INITIAL_RUB - valueToExchange, newRub);
        Assert.assertEquals(BalanceManager.INITIAL_EUR + valueToExchange / EUR_RUB, newUsd);
    }

    @Test
    public void textEchangeUsdToRub() {
        int valueToExchange = 500;
        boolean exchange = mBalanceManager.exchange(Currency.USD, Currency.RUB, valueToExchange);
        Assert.assertTrue(exchange);
        float newRub = mBalanceManager.getCurrencyBalance(Currency.RUB);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.USD);
        Assert.assertEquals(BalanceManager.INITIAL_RUB + valueToExchange * USD_RUB, newRub);
        Assert.assertEquals(BalanceManager.INITIAL_USD - valueToExchange, newUsd);
    }

    @Test
    public void textEchangeUsdToRubAll() {
        int valueToExchange = -1;
        boolean exchange = mBalanceManager.exchange(Currency.USD, Currency.RUB, valueToExchange);
        Assert.assertTrue(exchange);
        float newRub = mBalanceManager.getCurrencyBalance(Currency.RUB);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.USD);
        Assert.assertEquals(BalanceManager.INITIAL_RUB + BalanceManager.INITIAL_USD * USD_RUB, newRub);
        Assert.assertEquals(0f, newUsd);
    }

    @Test
    public void textEchangeUsdToEur() {
        int valueToExchange = 500;
        boolean exchange = mBalanceManager.exchange(Currency.USD, Currency.EUR, valueToExchange);
        Assert.assertTrue(exchange);
        float newEur = mBalanceManager.getCurrencyBalance(Currency.EUR);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.USD);
        Assert.assertEquals(BalanceManager.INITIAL_EUR + valueToExchange / EUR_USD, newEur);
        Assert.assertEquals(BalanceManager.INITIAL_USD - valueToExchange, newUsd);
    }

    @Test
    public void textEchangeEurToRub() {
        int valueToExchange = 500;
        boolean exchange = mBalanceManager.exchange(Currency.EUR, Currency.RUB, valueToExchange);
        Assert.assertTrue(exchange);
        float newRub = mBalanceManager.getCurrencyBalance(Currency.RUB);
        float newEur = mBalanceManager.getCurrencyBalance(Currency.EUR);
        Assert.assertEquals(BalanceManager.INITIAL_RUB + valueToExchange * EUR_RUB, newRub);
        Assert.assertEquals(BalanceManager.INITIAL_EUR - valueToExchange, newEur);
    }

    @Test
    public void textEchangeEurToUsd() {
        int valueToExchange = 500;
        boolean exchange = mBalanceManager.exchange(Currency.EUR, Currency.USD, valueToExchange);
        Assert.assertTrue(exchange);
        float newEur = mBalanceManager.getCurrencyBalance(Currency.EUR);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.USD);
        Assert.assertEquals(BalanceManager.INITIAL_EUR - valueToExchange, newEur);
        Assert.assertEquals(BalanceManager.INITIAL_USD + valueToExchange * EUR_USD, newUsd);
    }

    // Test valid excahnges

    @Test
    public void textEchangeError() {
        float balanceOld = mBalanceManager.getCurrencyBalance(Currency.RUB);
        boolean exchange = mBalanceManager.exchange(Currency.RUB, Currency.USD, BalanceManager.INITIAL_RUB * 2);
        Assert.assertFalse(exchange);
        float balanceNew = mBalanceManager.getCurrencyBalance(Currency.RUB);
        Assert.assertEquals(balanceOld, balanceNew);
    }

    @Test
    public void textExchangeWithInvalidRates() {
        Rates mockRates = PowerMockito.mock(Rates.class);
        when(mockRates.isUpToDate()).thenReturn(true);
        when(mockRates.isValid(RuntimeEnvironment.application)).thenReturn(false);

        when(mockRates.getUsdToRub()).thenReturn(USD_RUB);
        when(mockRates.getEurToRub()).thenReturn(EUR_RUB);
        when(mockRates.getUsdToEur()).thenReturn(EUR_USD);

        RatesUpdater mockRatesUpdated = PowerMockito.mock(RatesUpdater.class);
        when(mockRatesUpdated.getRates()).thenReturn(Observable.just(mockRates));

        mBalanceManager = new BalanceManager(RuntimeEnvironment.application, mockRealm, mockRatesUpdated);

        mBalanceManager.startRatesUpdater();

        int valueToExchange = 30000;
        boolean exchange = mBalanceManager.exchange(Currency.RUB, Currency.USD, valueToExchange);
        Assert.assertFalse(exchange);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.USD);
        Assert.assertEquals(BalanceManager.INITIAL_USD, newUsd);
    }

    @Test
    public void textExchangeWithOutDateRates() {
        Rates mockRates = PowerMockito.mock(Rates.class);
        when(mockRates.isUpToDate()).thenReturn(false);
        when(mockRates.isValid(RuntimeEnvironment.application)).thenReturn(true);

        when(mockRates.getUsdToRub()).thenReturn(USD_RUB);
        when(mockRates.getEurToRub()).thenReturn(EUR_RUB);
        when(mockRates.getUsdToEur()).thenReturn(EUR_USD);

        RatesUpdater mockRatesUpdated = PowerMockito.mock(RatesUpdater.class);
        when(mockRatesUpdated.getRates()).thenReturn(Observable.just(mockRates));

        mBalanceManager = new BalanceManager(RuntimeEnvironment.application, mockRealm, mockRatesUpdated);

        mBalanceManager.startRatesUpdater();

        int valueToExchange = 30000;
        boolean exchange = mBalanceManager.exchange(Currency.RUB, Currency.USD, valueToExchange);
        Assert.assertFalse(exchange);
        float newUsd = mBalanceManager.getCurrencyBalance(Currency.USD);
        Assert.assertEquals(BalanceManager.INITIAL_USD, newUsd);
    }

}