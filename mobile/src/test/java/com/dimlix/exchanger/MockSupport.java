package com.dimlix.exchanger;

import com.dimlix.exchanger.model.Balance;

import org.powermock.api.mockito.PowerMockito;

import io.realm.Realm;
import io.realm.RealmQuery;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by dryaz on 02.03.16.
 */
public class MockSupport {
    public static Realm mockRealm() {
        mockStatic(Realm.class);

        Realm mockRealm = PowerMockito.mock(Realm.class);

        when(mockRealm.createObject(Balance.class)).thenReturn(new Balance(BalanceManager.INITIAL_RUB,
                BalanceManager.INITIAL_USD, BalanceManager.INITIAL_EUR));
        when(mockRealm.where(Balance.class)).thenReturn(PowerMockito.mock(RealmQuery.class));

        when(Realm.getDefaultInstance()).thenReturn(mockRealm);

        return mockRealm;
    }
}
