package com.dimlix.exchanger.model;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.dimlix.exchanger.R;
import com.dimlix.exchanger.utils.SecUtils;
import com.google.gson.annotations.SerializedName;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Created by dryaz on 22.02.16.
 */
public class Rates {
    private static final String TEMP = "(H@&#%()%^Rgkl";
    @SerializedName("ts")
    long timeStamp;
    @SerializedName("USDRUB")
    float usdToRub;
    @SerializedName("EURUSD")
    float eurToUsd;
    @SerializedName("EURRUB")
    float eurToRub;
    @SerializedName("USDRUB_DEF")
    float usdToRubDyn;
    @SerializedName("EURRUB_DEF")
    float eurToRubDyn;
    @SerializedName("sec")
    String sec;

    @Override
    public String toString() {
        return "Rates{" +
                "timeStamp=" + timeStamp +
                ", usdToRub=" + usdToRub +
                ", eurToUsd=" + eurToUsd +
                ", eurToRub=" + eurToRub +
                ", usdToRubDyn=" + usdToRubDyn +
                ", eurToRubDyn=" + eurToRubDyn +
                ", sec='" + sec + '\'' +
                '}';
    }

    public Rates(final long timeStamp,
                 final float usdToRub,
                 final float eurToUsd,
                 final float eurToRub,
                 final int usdToRubDyn,
                 final int eurToRubDyn,
                 final String sec) {
        this.timeStamp = timeStamp;
        this.usdToRub = usdToRub;
        this.eurToUsd = eurToUsd;
        this.eurToRub = eurToRub;
        this.usdToRubDyn = usdToRubDyn;
        this.eurToRubDyn = eurToRubDyn;
        this.sec = sec;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public float getUsdToRub() {
        return usdToRub;
    }

    public float getUsdToEur() {
        return eurToUsd;
    }

    public float getEurToRub() {
        return eurToRub;
    }

    public float getUsdToRubDyn() {
        return usdToRubDyn;
    }

    public float getEurToRubDyn() {
        return eurToRubDyn;
    }

    public String getSec() {
        return sec;
    }

    public boolean isValid(Context context) {
        String salt = "2935!8gn4" + getPart() + "@%jY(!YfOP@$fH" + context.getString(R.string.developer) + SecUtils.getVal() + "ghi2@g@$Gf" + TEMP;
        String digest = null;
        try {
            digest = md5(salt) + timeStamp;
            digest = md5(digest) + String.format(Locale.US, "%.4f", eurToUsd);
            digest = md5(digest) + String.format(Locale.US, "%.4f", usdToRub);
            digest = md5(digest) + String.format(Locale.US, "%.4f", eurToRub);
            return !TextUtils.isEmpty(sec) && sec.equals(md5(digest));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isUpToDate() {
        return System.currentTimeMillis() / 1000 - timeStamp < 60;
    }

    public static String md5(String input) throws NoSuchAlgorithmException {
        String result = input;
        if (input != null) {
            MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while (result.length() < 32) { //40 for SHA-1
                result = "0" + result;
            }
        }
        return result;
    }

    private String getPart() {
        return "hg902shi";
    }
}
