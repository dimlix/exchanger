package com.dimlix.exchanger.model;

import android.support.annotation.IntDef;

import com.dimlix.exchanger.ui.CurrencySpinnerAdapter;

/**
 * Created by dryaz on 06.02.16.
 */
@IntDef({Currency.RUB, Currency.USD, Currency.EUR})
public @interface Currency {
    int RUB = 0;
    int USD = 1;
    int EUR = 2;
}
