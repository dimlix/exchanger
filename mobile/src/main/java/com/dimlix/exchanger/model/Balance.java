package com.dimlix.exchanger.model;

import io.realm.RealmObject;

/**
 * Created by dryaz on 02.03.16.
 */
public class Balance extends RealmObject {
    private float rub;
    private float usd;
    private float eur;

    public Balance() {
    }

    public Balance(final float rub, final float usd, final float eur) {
        this.rub = rub;
        this.usd = usd;
        this.eur = eur;
    }

    public float getRub() {
        return rub;
    }

    public void setRub(final float rub) {
        this.rub = rub;
    }

    public float getUsd() {
        return usd;
    }

    public void setUsd(final float usd) {
        this.usd = usd;
    }

    public float getEur() {
        return eur;
    }

    public void setEur(final float eur) {
        this.eur = eur;
    }
}
