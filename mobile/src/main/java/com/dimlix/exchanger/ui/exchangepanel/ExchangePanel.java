package com.dimlix.exchanger.ui.exchangepanel;

import android.view.View;

/**
 * Created by dryaz on 06.02.16.
 */
public interface ExchangePanel {
    View getView();
}
