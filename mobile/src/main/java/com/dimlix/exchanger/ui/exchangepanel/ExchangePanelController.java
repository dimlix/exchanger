package com.dimlix.exchanger.ui.exchangepanel;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dimlix.exchanger.R;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dryaz on 06.02.16.
 */
public class ExchangePanelController {

    private final ExchangePanelAdapter mAdapter;

    @Bind(R.id.exchange_panel_pager)
    ViewPager mViewPager;
    @Bind(R.id.left_arrow)
    AppCompatImageButton mLeftArrow;
    @Bind(R.id.right_arrow)
    AppCompatImageButton mRightArrow;

    public ExchangePanelController(Activity context, BaseExchangePanel.Listener listener) {
        ButterKnife.bind(this, context);
        List<ExchangePanel> panels = new ArrayList<>();
        panels.add(new FixExchangePanel(context, listener));
        panels.add(new CustomExchangePanel(context, listener));
        mAdapter = new ExchangePanelAdapter(panels);
        mViewPager.setAdapter(mAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position,
                                       final float positionOffset,
                                       final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                boolean isFirst = position == 0;
                boolean isLast = position == mAdapter.getCount() - 1;
                if (isFirst) {
                    mLeftArrow.setImageResource(R.drawable.left_inactive);
                    mRightArrow.setImageResource(R.drawable.right_active);
                } else if (isLast) {
                    mLeftArrow.setImageResource(R.drawable.left_active);
                    mRightArrow.setImageResource(R.drawable.right_inactive);
                } else {
                    mLeftArrow.setImageResource(R.drawable.left_active);
                    mRightArrow.setImageResource(R.drawable.right_active);
                }
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        RxView.clicks(mLeftArrow).subscribe(aVoid -> changePages(-1));
        RxView.clicks(mRightArrow).subscribe(aVoid -> changePages(1));
    }

    private void changePages(int numPages) {
        int desiredPage = mViewPager.getCurrentItem() + numPages;
        if (desiredPage < 0) {
            desiredPage = 0;
        }
        if (desiredPage > mAdapter.getCount() - 1) {
            desiredPage = mAdapter.getCount() - 1;
        }
        mViewPager.setCurrentItem(desiredPage);
    }

    private static class ExchangePanelAdapter extends PagerAdapter {

        List<ExchangePanel> mPanels = null;

        public ExchangePanelAdapter(List<ExchangePanel> panels){
            mPanels = panels;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = mPanels.get(position).getView();
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(final ViewGroup container, final int position, final Object object) {
            container.removeViewAt(position);
        }

        @Override
        public int getCount(){
            return mPanels.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object){
            return view.equals(object);
        }
    }
}
