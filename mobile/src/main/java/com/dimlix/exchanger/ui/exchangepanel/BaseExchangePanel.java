package com.dimlix.exchanger.ui.exchangepanel;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.dimlix.exchanger.R;
import com.dimlix.exchanger.model.Currency;
import com.dimlix.exchanger.ui.CurrencySpinnerAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dryaz on 06.02.16.
 */
public abstract class BaseExchangePanel implements ExchangePanel {
    private static final String WITHDRAW_SEL_KEY = "wsk";
    private static final String DEPOSIT_SEL_KEY = "dsk";

    private final View mView;
    protected final Listener mListener;
    @Bind(R.id.withdraw_spinner)
    protected Spinner mWithdrawSpinner;
    @Bind(R.id.deposit_spinner)
    protected Spinner mDepositSpinner;

    protected int mLastWithdrawSelection = 0;
    protected int mLastDepositSelection = 1;

    public BaseExchangePanel(final Context context, Listener listener) {
        mListener = listener;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(getParentViewId(), null);
        mView = view;
        ButterKnife.bind(this, view);

        mWithdrawSpinner.setAdapter(new CurrencySpinnerAdapter(context));
        mDepositSpinner.setAdapter(new CurrencySpinnerAdapter(context));

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        mLastWithdrawSelection = preferences.getInt(WITHDRAW_SEL_KEY, 0);
        mLastDepositSelection = preferences.getInt(DEPOSIT_SEL_KEY, 1);

        mWithdrawSpinner.setSelection(mLastWithdrawSelection);
        mDepositSpinner.setSelection(mLastDepositSelection);

        mWithdrawSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                if (position == mLastDepositSelection) {
                    mDepositSpinner.setSelection(mLastWithdrawSelection, true);
                }
                mLastWithdrawSelection = position;
                onSellCurrencyChanged(position);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });

        mDepositSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                if (position == mLastWithdrawSelection) {
                    mWithdrawSpinner.setSelection(mLastDepositSelection, true);
                }
                mLastDepositSelection = position;
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });
    }

    protected void onSellCurrencyChanged(@Currency int currency) {
        // Override if neccessary.
    }

    @Override
    public View getView() {
        return mView;
    }

    protected abstract int getParentViewId();

    public interface Listener {
        void onExchangeInit(@Currency int from, @Currency int to, float value);
    }
}
