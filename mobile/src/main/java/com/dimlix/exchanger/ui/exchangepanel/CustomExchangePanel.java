package com.dimlix.exchanger.ui.exchangepanel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;

import com.dimlix.exchanger.R;
import com.dimlix.exchanger.ui.CurrencySpinnerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dryaz on 06.02.16.
 */
public class CustomExchangePanel extends BaseExchangePanel {

    public CustomExchangePanel(final Context context, Listener listener) {
        super(context, listener);
    }

    @Override
    protected int getParentViewId() {
        return R.layout.custom_exchange;
    }
}
