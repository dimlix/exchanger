package com.dimlix.exchanger.ui.exchangepanel;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.dimlix.exchanger.R;
import com.dimlix.exchanger.model.Currency;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * Created by dryaz on 06.02.16.
 */
public class FixExchangePanel extends BaseExchangePanel {

    @Bind(R.id.viceversa)
    View mViceVersa;

    @Bind(R.id.button_first)
    AppCompatButton mButtonFirst;
    @Bind(R.id.button_second)
    AppCompatButton mButtonSecond;
    @Bind(R.id.button_third)
    AppCompatButton mButtonThird;

    @Bind(R.id.button_all)
    View mButtonAll;

    private final Map<Integer, List<CurrencyButton>> mCurrencyButtons = new HashMap<>();

    @SuppressWarnings("WrongConstant")
    public FixExchangePanel(final Context context, Listener listener) {
        super(context, listener);
        RxView.clicks(mViceVersa).subscribe(aVoid -> mWithdrawSpinner.setSelection(mLastDepositSelection, true));
        List<CurrencyButton> rubs = new ArrayList<>();
        rubs.add(new CurrencyButton("1k", 1000f));
        rubs.add(new CurrencyButton("10k", 10000f));
        rubs.add(new CurrencyButton("100k", 10000f));
        List<CurrencyButton> usds = new ArrayList<>();
        usds.add(new CurrencyButton("100", 100f));
        usds.add(new CurrencyButton("1k", 1000f));
        usds.add(new CurrencyButton("10k", 10000f));
        List<CurrencyButton> eur = new ArrayList<>();
        eur.add(new CurrencyButton("100", 100f));
        eur.add(new CurrencyButton("1k", 1000f));
        eur.add(new CurrencyButton("10k", 10000f));
        mCurrencyButtons.put(Currency.RUB, rubs);
        mCurrencyButtons.put(Currency.USD, usds);
        mCurrencyButtons.put(Currency.EUR, eur);

        RxView.clicks(mButtonFirst).subscribe(aVoid -> mListener.onExchangeInit(mLastWithdrawSelection, mLastDepositSelection, mCurrencyButtons.get(mLastWithdrawSelection).get(0).getValue()));
        RxView.clicks(mButtonSecond).subscribe(aVoid -> mListener.onExchangeInit(mLastWithdrawSelection, mLastDepositSelection, mCurrencyButtons.get(mLastWithdrawSelection).get(1).getValue()));
        RxView.clicks(mButtonThird).subscribe(aVoid -> mListener.onExchangeInit(mLastWithdrawSelection, mLastDepositSelection, mCurrencyButtons.get(mLastWithdrawSelection).get(2).getValue()));
        RxView.clicks(mButtonAll).subscribe(aVoid -> mListener.onExchangeInit(mLastWithdrawSelection, mLastDepositSelection, -1));
    }

    @Override
    protected void onSellCurrencyChanged(@Currency final int currency) {
        super.onSellCurrencyChanged(currency);
        mButtonFirst.setText(mCurrencyButtons.get(currency).get(0).getTitle());
        mButtonSecond.setText(mCurrencyButtons.get(currency).get(1).getTitle());
        mButtonThird.setText(mCurrencyButtons.get(currency).get(2).getTitle());
    }

    @Override
    protected int getParentViewId() {
        return R.layout.fix_exchange;
    }

    private static class CurrencyButton {
        private final String mTitle;
        private final float mValue;

        public CurrencyButton(final String title, final float value) {
            mTitle = title;
            mValue = value;
        }

        public String getTitle() {
            return mTitle;
        }

        public float getValue() {
            return mValue;
        }
    }
}
