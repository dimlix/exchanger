package com.dimlix.exchanger.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

/**
 * Created by dryaz on 28.12.15.
 */
public class LoseFocusEditText extends EditText {
    public LoseFocusEditText(final Context context) {
        super(context);
    }

    public LoseFocusEditText(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public LoseFocusEditText(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LoseFocusEditText(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            clearFocus();
        }
        return super.dispatchKeyEvent(event);
    }
}
