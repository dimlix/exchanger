package com.dimlix.exchanger.ui;

import android.content.Context;
import android.support.annotation.IntDef;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.dimlix.exchanger.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dryaz on 06.02.16.
 */
public class CurrencySpinnerAdapter extends ArrayAdapter<Integer> {
    private static final Integer[] IMAGES;
    private Map<Integer, View> mViews = new HashMap<>();

    static {
        IMAGES = new Integer[]{R.drawable.ruble, R.drawable.dollar, R.drawable.euro};
    }

    public CurrencySpinnerAdapter(Context context) {
        super(context, android.R.layout.simple_spinner_item, IMAGES);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getImageForPosition(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mViews.get(position);
        if (view == null) {
            View viewForPosition = getImageForPosition(position);
            mViews.put(position, viewForPosition);
            return viewForPosition;
        }
        return view;
    }

    private View getImageForPosition(int position) {
        int size = getContext().getResources().getDimensionPixelSize(R.dimen.clickable_size);
        ImageView imageView = new ImageView(getContext());
        imageView.setBackgroundResource(IMAGES[position]);
        imageView.setLayoutParams(new AbsListView.LayoutParams(size, size));
        return imageView;
    }
}
