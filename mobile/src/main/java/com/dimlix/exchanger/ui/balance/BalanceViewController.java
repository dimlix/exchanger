package com.dimlix.exchanger.ui.balance;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.TextView;

import com.dimlix.exchanger.BalanceManager;
import com.dimlix.exchanger.R;
import com.dimlix.exchanger.model.Currency;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dryaz on 09.03.16.
 */
public class BalanceViewController {
    public static final Float MILLION = 1000000f;
    public static final Float BILLION = 1000000000f;
    public static final Float TRILLION = 1000000000000f;

    private final BalanceManager mBalanceManager;
    private final DecimalFormat mDecimalFormat;
    private final Context mContext;

    @Bind(R.id.rub_current)
    TextView mRubCurrent;
    @Bind(R.id.rub_total)
    TextView mRubTotal;
    @Bind(R.id.usd_current)
    TextView mUsdCurrent;
    @Bind(R.id.usd_total)
    TextView mUsdTotal;
    @Bind(R.id.eur_current)
    TextView mEurCurrent;
    @Bind(R.id.eur_total)
    TextView mEurTotal;

    public BalanceViewController(BalanceManager balanceManager, View parent) {
        ButterKnife.bind(this, parent);
        mContext = parent.getContext();
        mBalanceManager = balanceManager;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        mDecimalFormat = new DecimalFormat("#,##0.0000", symbols);
    }

    public void updateBalance() {
        mRubCurrent.setText(getStringValue(mBalanceManager.getCurrencyBalance(Currency.RUB), false));
        mUsdCurrent.setText(getStringValue(mBalanceManager.getCurrencyBalance(Currency.USD), false));
        mEurCurrent.setText(getStringValue(mBalanceManager.getCurrencyBalance(Currency.EUR), false));

        mRubTotal.setText(getStringValue(mBalanceManager.getTotal(Currency.RUB), true));
        mUsdTotal.setText(getStringValue(mBalanceManager.getTotal(Currency.USD), true));
        mEurTotal.setText(getStringValue(mBalanceManager.getTotal(Currency.EUR), true));
    }

    private SpannableString getStringValue(float value, boolean minimize) {
        if (minimize) {
            if (value >= TRILLION) {
                return SpannableString.valueOf(mContext.getString(R.string.trillion, Math.floor(value / TRILLION)));
            }
            if (value >= BILLION) {
                return SpannableString.valueOf(mContext.getString(R.string.billion, Math.floor(value / BILLION)));
            }
            if (value >= MILLION) {
                return SpannableString.valueOf(mContext.getString(R.string.million, Math.floor(value / MILLION)));
            }
        }
        if (value < 0) {
            return SpannableString.valueOf("-");
        }
        String finalValue = mDecimalFormat.format(value);
        SpannableString spanableString = new SpannableString(finalValue);
        spanableString.setSpan(new RelativeSizeSpan(0.7f), finalValue.indexOf('.'), finalValue.length(), 0);
        return spanableString;
    }
}
