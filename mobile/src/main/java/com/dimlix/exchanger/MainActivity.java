package com.dimlix.exchanger;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.dimlix.exchanger.model.Rates;
import com.dimlix.exchanger.network.RatesUpdater;
import com.dimlix.exchanger.ui.balance.BalanceViewController;
import com.dimlix.exchanger.ui.exchangepanel.BaseExchangePanel;
import com.dimlix.exchanger.ui.exchangepanel.ExchangePanelController;
import com.dimlix.exchanger.utils.StringUtils;
//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Logger;
//import com.google.android.gms.analytics.Tracker;

import java.text.DecimalFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

public class MainActivity extends AppCompatActivity {

    private final DecimalFormat mDecimalFormat = new DecimalFormat("+#,##0.0000;-#");

    private Drawable positivDynamic;
    private Drawable negativeDynamic;

    private MenuItem mRefresh;

    @Bind(R.id.rub_usd_rate)
    TextView rubUsdRate;
    @Bind(R.id.rub_eur_rate)
    TextView rubEurRate;

    @Bind(R.id.rub_usd_diff)
    TextView mRubUsdRateDiff;
    @Bind(R.id.rub_eur_diff)
    TextView mRubEurRateDiff;

    private BalanceManager mBalanceManager;
    private BalanceViewController mBalanceViewController;

    private CompositeSubscription mSubscription = new CompositeSubscription();
    private ExchangePanelController mExchangePanelController;
//    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        positivDynamic = ContextCompat.getDrawable(MainActivity.this, R.drawable.up);
        negativeDynamic = ContextCompat.getDrawable(MainActivity.this, R.drawable.down);

        mBalanceManager = new BalanceManager(MainActivity.this, Realm.getInstance(MainActivity.this), new RatesUpdater());
        mBalanceManager.startRatesUpdater();

        mBalanceViewController = new BalanceViewController(mBalanceManager, findViewById(R.id.balance_view));

        mExchangePanelController = new ExchangePanelController(MainActivity.this, (from, to, value) -> {
            boolean success = mBalanceManager.exchange(from, to, value);
            if (success) {
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("FixEchange")
//                        .setAction(StringUtils.currencyExchangeToString(from, to))
//                        .setLabel("OK")
//                        .setValue((long) value)
//                        .build());
            } else {
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("FixEchange")
//                        .setAction(StringUtils.currencyExchangeToString(from, to))
//                        .setLabel("NotEnough")
//                        .setValue((long) value)
//                        .build());
            }
        });

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
//        mTracker = application.getDefaultTracker();
//
//        GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSubscription.add(mBalanceManager.getRatesObserveable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateRates, throwable -> updateRatesProblem()));
        mSubscription.add(mBalanceManager.getBalanceObserveable()
                .subscribe(balance -> mBalanceViewController.updateBalance()));
    }

    @Override
    protected void onStop() {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mBalanceManager.destroy();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mRefresh = menu.findItem(R.id.action_refresh);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                mRefresh.setVisible(false);
                mBalanceManager.startRatesUpdater();
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("Rates")
//                        .setAction("ClickRefresh")
//                        .build());
                break;
            case R.id.action_highscore:
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("Highscores")
//                        .setAction("ClickAB")
//                        .build());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBalanceManager.startRatesUpdater();
    }

    private void updateDynamic(TextView tv, float dyn) {
        if (dyn > 0) {
            tv.setCompoundDrawablesWithIntrinsicBounds(tv.getCompoundDrawables()[0], null, positivDynamic, null);
        } else if (dyn < 0) {
            tv.setCompoundDrawablesWithIntrinsicBounds(tv.getCompoundDrawables()[0], null, negativeDynamic
                    , null);
        }
    }

    private void updateRates(Rates rates) {
        if (rates.isValid(MainActivity.this)) {
            rubUsdRate.setText(getString(R.string.rub_val, rates.getUsdToRub()));
            rubEurRate.setText(getString(R.string.rub_val, rates.getEurToRub()));
            float usdToRubDyn = rates.getUsdToRubDyn();
            if (usdToRubDyn != 0) {
                mRubUsdRateDiff.setText(mDecimalFormat.format(usdToRubDyn));
            }
            float eurToRubDyn = rates.getEurToRubDyn();
            if (eurToRubDyn != 0) {
                mRubEurRateDiff.setText(mDecimalFormat.format(eurToRubDyn));
            }
            updateDynamic(rubUsdRate, usdToRubDyn);
            updateDynamic(rubEurRate, eurToRubDyn);
            mBalanceViewController.updateBalance();
        } else {
            updateRatesProblem();
//            mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory("Rates")
//                    .setAction("GetInvalidRates")
//                    .build());
        }
    }

    private void updateRatesProblem() {
        showNetworkProblem();
        mRefresh.setVisible(true);
        rubUsdRate.setText("-");
        rubEurRate.setText("-");
    }

    private void showNetworkProblem() {
        Toast.makeText(MainActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
