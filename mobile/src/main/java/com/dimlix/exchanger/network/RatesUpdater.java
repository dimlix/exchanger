package com.dimlix.exchanger.network;

import com.dimlix.exchanger.model.Rates;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by dryaz on 03.03.16.
 */
public class RatesUpdater {

    private final Observable<Rates> mRatesObservable;
    private final RatesService mService;

    public RatesUpdater() {
        mService = ServiceFactory.createRetrofitService(RatesService.class,
                RatesService.SERVICE_ENDPOINT);
        mRatesObservable = Observable.interval(0, 10, TimeUnit.SECONDS)
                .flatMap(n ->
                        mService.getRates()
                                .retryWhen(new RetryWithDelay(3, 3000))
                                .subscribeOn(Schedulers.io()));
    }

    public Observable<Rates> getRates() {
        return mRatesObservable;
    }
}
