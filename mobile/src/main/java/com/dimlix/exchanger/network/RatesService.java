package com.dimlix.exchanger.network;

import com.dimlix.exchanger.model.Rates;

import retrofit.http.GET;
import rx.Observable;

/**
 * Created by dryaz on 22.02.16.
 */
public interface RatesService {
    String SERVICE_ENDPOINT = "http://dimlix.com";

    @GET("/currency/cur-rate.php?json")
    Observable<Rates> getRates();
}
