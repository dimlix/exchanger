package com.dimlix.exchanger;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.dimlix.exchanger.model.Balance;
import com.dimlix.exchanger.model.Currency;
import com.dimlix.exchanger.model.Rates;
import com.dimlix.exchanger.network.RatesUpdater;

import io.realm.Realm;
import rx.Observable;

/**
 * Created by dryaz on 02.03.16.
 */
public class BalanceManager {

    public static final float INITIAL_RUB = 100000;
    public static final float INITIAL_USD = 2000;
    public static final float INITIAL_EUR = 1000;

    private final Balance mBalance;
    private final Context mContext;

    private RatesUpdater mRatesUpdater;
    private Realm mRealm;
    private Rates mRates;
    private Observable<Rates> mRatesObservable;

    public BalanceManager(Context context, Realm realm, RatesUpdater ratesUpdater) {
        mContext = context;
        mRealm = realm;
        Balance balance = mRealm.where(Balance.class).findFirst();
        if (balance == null) {
            mRealm.beginTransaction();
            balance = mRealm.createObject(Balance.class);
            balance.setRub(INITIAL_RUB);
            balance.setUsd(INITIAL_USD);
            balance.setEur(INITIAL_EUR);
            mRealm.commitTransaction();
        }
        mBalance = balance;
        mRatesUpdater = ratesUpdater;
    }

    public void startRatesUpdater() {
        mRatesObservable = mRatesUpdater.getRates().doOnNext(rates -> mRates = rates);
    }

    public float getTotal(@Currency int currency) {
        if (mRates == null) return -1;
        switch (currency) {
            case Currency.RUB:
                return mBalance.getRub() + mBalance.getEur()* mRates.getEurToRub() + mBalance.getUsd()* mRates.getUsdToRub();
            case Currency.USD:
                return mBalance.getUsd() + mBalance.getRub()/ mRates.getUsdToRub() + mBalance.getEur()* mRates.getUsdToEur();
            case Currency.EUR:
                return mBalance.getEur() + mBalance.getRub()/ mRates.getEurToRub() + mBalance.getUsd()/ mRates.getUsdToEur();
        }
        return -1;
    }

    public float getCurrencyBalance(@Currency int currency) {
        switch (currency) {
            case Currency.RUB:
                return mBalance.getRub();
            case Currency.USD:
                return mBalance.getUsd();
            case Currency.EUR:
                return mBalance.getEur();
        }
        return 0;
    }

    public Balance getBalance() {
        return mBalance;
    }

    public boolean exchange(@Currency int from, @Currency int to, float value) {
        if (mRates == null || !mRates.isValid(mContext) || !mRates.isUpToDate()) {
            return false;
        }
        float fromValue = getCurrencyBalance(from);
        float toValue = getCurrencyBalance(to);
        if (value > fromValue) {
            return false;
        }
        if (value == -1) {
            value = fromValue;
        }
        mRealm.beginTransaction();
        setBalanceValue(from, fromValue - value);
        setBalanceValue(to, toValue + value * getRates(from, to));
        mRealm.commitTransaction();
        return true;
    }

    private void setBalanceValue(@Currency int currency, float value) {
        switch (currency) {
            case Currency.RUB: mBalance.setRub(value); break;
            case Currency.USD: mBalance.setUsd(value); break;
            case Currency.EUR: mBalance.setEur(value); break;
        }
    }

    @VisibleForTesting
    float getRates(@Currency int from, @Currency int to) {
        if (mRates == null) {
            throw new IllegalStateException("Update rates first");
        }
        if (from == Currency.RUB) {
            if (to == Currency.USD) {
                return 1f / mRates.getUsdToRub();
            } else if (to == Currency.EUR) {
                return 1f / mRates.getEurToRub();
            }
        } else if (from == Currency.USD) {
            if (to == Currency.RUB) {
                return mRates.getUsdToRub();
            } else if (to == Currency.EUR) {
                return 1f / mRates.getUsdToEur();
            }
        } else if (from == Currency.EUR) {
            if (to == Currency.USD) {
                return mRates.getUsdToEur();
            } else if (to == Currency.RUB) {
                return mRates.getEurToRub();
            }
        }
        throw new IllegalStateException("Illegal currency values");
    }

    public void destroy() {
        if (!mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public Observable<Balance> getBalanceObserveable() {
        return mBalance.asObservable();
    }

    public Observable<Rates> getRatesObserveable() {
        return mRatesObservable;
    }

}
