package com.dimlix.exchanger.utils;

import com.dimlix.exchanger.model.Currency;

/**
 * Created by dryaz on 14.03.16.
 */
public class StringUtils {
    public static final String currencyToString(@Currency int currency) {
        switch (currency) {
            case Currency.RUB: return "rub";
            case Currency.USD: return "usd";
            case Currency.EUR: return "eur";
        }
        return "unknown";
    }

    public static final String currencyExchangeToString(@Currency int from, @Currency int to) {
        return currencyToString(from) + '>' + currencyToString(to);
    }
}
