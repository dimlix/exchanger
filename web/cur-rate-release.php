<?php
  define('CP_TIMER', microtime(true));        //Таймер
  define('CP_DB_HOST', 'localhost');          //Хост БД
  define('CP_DB_USER', 'u5501505_currate');   //Пользователь БД
  define('CP_DB_PASS', 'Rm88Fz0p');           //Пароль БД
  define('CP_DB_NAME', 'u5501505_currate');   //Название БД
  define('CP_EXEC_TIME', 58);                 //Время выполнения скрпта
  define('CP_SLEEP_SEC', 12);                  //С каким интервалом опрашивать сервер
  define('CP_SAVE_AFTER', 1);                 //Сохранять в БД каждый Х запрос
  define('CP_SHMEM_ID', 29645);               //ID блока shared memory
  set_time_limit(60);
  if ($_SERVER['argc'] > 1) {
    for($i = 1; $i < $_SERVER['argc']; $i++) $_GET[$_SERVER['argv'][$i]] = '';
  }
  $hMem = shmop_open(CP_SHMEM_ID, 'c', 0644, 1024);
  $aLRate = @unserialize(shmop_read($hMem, 0, 1024));
  if (isset($_GET['cron'])) {
    $hSock = fsockopen('query.yahooapis.com', 80, $iErr, $sErr, 2);
    if (!$hSock) exit;
    $aSQL = array();
    $iNum = 1;
    while (microtime(true) - CP_TIMER < CP_EXEC_TIME) {
      $aRate = getCurrentRateData($hSock);
      foreach ($aRate as $sKey => $mVal) {
        if ($sKey == 'ts' || strlen($sKey) > 6) continue;
        $aRate[$sKey . '_DEF'] = $mVal > $aLRate[$sKey] ? 1 : ($mVal < $aLRate[$sKey] ? -1 : 0);
      }
      $aLRate = $aRate;
      shmop_write($hMem, serialize($aRate), 0);
      if ($iNum % CP_SAVE_AFTER == 0) {
        foreach ($aRate as $sKey => $mVal) {
          if ($sKey == 'ts') continue;
          $aSQL[] = "('{$sKey}', {$mVal}, '{$aRate['ts']}')";
        }
      }
      $iNum++;
      sleep(CP_SLEEP_SEC);
    }
    fclose($hSock);
    $hDB = new mysqli(CP_DB_HOST, CP_DB_USER, CP_DB_PASS, CP_DB_NAME);
    $hDB->set_charset('utf-8');
    $hDB->query('INSERT INTO `rate_history_release` (`key`, `rate`, `time`) VALUES ' . implode(',', $aSQL));
    $hDB->close();
    exit;
  }
  if (isset($_GET['json'])) {
    $aRate = @unserialize(shmop_read($hMem, 0, 1024));
    $tohash = '2935!8gn4hg902shi@%jY(!YfOP@$fHdilix24089ghi2@g@$Gf(H@&#%()%^Rgkl';
    foreach ($aRate as $sKey => $mVal) {
      if (strlen($sKey) > 6) continue;
      if ($sKey == 'ts') {
           $tohash = md5($tohash) . strtotime($mVal);
           continue;
      }
      $tohash = md5($tohash) . $mVal;
    }
    $aRate['ts'] = strtotime($aRate['ts']);
    $aRate['sec'] = md5($tohash);

    echo json_encode($aRate);
  }
  shmop_close($hMem);

  function getCurrentRateData ($hSock) {
    fwrite($hSock, "GET /v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22EURUSD%22,%22USDRUB%22,%22EURRUB%22%29&env=store://datatables.org/alltableswithkeys&format=json HTTP/1.1\n");
    fwrite($hSock, "Host: query.yahooapis.com\n\n");
    $aData = json_decode(preg_replace('#^.*(\{"query".*\}\]\}\}\})[\s\d]*$#si', '$1', fread($hSock, 16384)), true);
    $aRes = array('ts' => str_replace(array('T', 'Z'), ' ', $aData['query']['created']));
    foreach ($aData['query']['results']['rate'] as $aCurr) {
      $aRes[$aCurr['id']] = number_format($aCurr['Rate'], 4, '.', '');
    }
    return $aRes;
  }
